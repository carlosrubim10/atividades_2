namespace exemplo_condicoes
{
    let idade: number = 10;

    if(idade >= 18)
    {
        console.log("Pode dirigir")
    }
    else
    {
        console.log("Não pode dirigir")
    }
}
/*{
    //Ternario]
    idade >= 18: console.log("Pode dirigir") : console.log console.log("Não pode dirigir")
}*/