/* Faça um programa que calcule a média das quatro notas de um aluno e informese ele foi aprovado ou reprovado. A nota de corte é de 6 pontos*/

namespace exercicio_1{

    let nota1, nota2, nota3, nota4, media: number;
    nota1 = 9;
    nota2 = 4;
    nota3 = 7;
    nota4 = 6;

    media = (nota1 + nota2 + nota3 + nota4) / 4

    if (media >= 6)
    {
        console.log("O aluno foi aprovado!");
    }   else {
        console.log("Aluno reprovado!");
    }
    }
