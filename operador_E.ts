//Testado o operador E -- &&
namespace operador_E{
    let idade = 20;
    let maioridade = idade > 18;
    let possuiCarteiraDeMotorista = true; 

    let podeDirigir = maioridade && possuiCarteiraDeMotorista

    console.log(podeDirigir); // true
}